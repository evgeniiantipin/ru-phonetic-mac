
## Russian phonetic (MacOS) keyboard layout for Linux 

For those of you who got used to the MacOS RU phonetic layout and wanted to use it on Linux machine.
I'm currently using this layout on my daily driver T470 with Fedora 27 (Wayland) installed on it.

1. Add definition from ru-phonetic-mac.xkb file in `/usr/share/X11/xkb/symbols/ru`

2. And register it in `/usr/share/X11/xkb/rules/evdev.xml`

<variant>
  <configItem>
    <name>njorak</name>
    <description>Swedish (Njorak, level3 dead keys)</description>
  </configItem>
</variant>

Configure gnome to use it

The relevant keys in gsettings are

/org/gnome/desktop/input-sources/sources
    list should include 'se+njorak'
/org/gnome/desktop/input-sources/xkb-options
    option to configure lv3 switch e.g "lv3:lalt_switch" 
